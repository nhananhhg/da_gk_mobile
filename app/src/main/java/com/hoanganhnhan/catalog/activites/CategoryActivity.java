package com.hoanganhnhan.catalog.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.hoanganhnhan.catalog.R;
import com.hoanganhnhan.catalog.adapters.BrandAdapter;
import com.hoanganhnhan.catalog.adapters.DemandAdapter;
import com.hoanganhnhan.catalog.datas.data;



public class CategoryActivity extends AppCompatActivity {

    RecyclerView rvBrand;
    data dataList = new data();
    BrandAdapter brandAdapter;
    RecyclerView rvDemand;
    DemandAdapter demandAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        setControl();
        setEvent();

    }
    public void setControl(){
        rvBrand = findViewById(R.id.rvBrand);
        rvDemand = findViewById(R.id.rvDemand);
    }

    public void setEvent(){
        setRvBrand();
        setRvDemand();
    }

    public void setRvBrand(){
        //        khoi tao gridlayout manager
        brandAdapter = new BrandAdapter(this);  //this: tham chieu den activity hien tai
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        rvBrand.setLayoutManager(gridLayoutManager);
        brandAdapter.setData(dataList.getListBrand());
        rvBrand.setAdapter(brandAdapter);
    }

    public void setRvDemand(){
        demandAdapter = new DemandAdapter(this);
        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(this, 3){
            @Override
            public boolean canScrollVertically(){
                return false;
            }
        };
        rvDemand.setLayoutManager(gridLayoutManager1);
        demandAdapter.setData(dataList.getListDemand());
        rvDemand.setAdapter(demandAdapter);
    }

}