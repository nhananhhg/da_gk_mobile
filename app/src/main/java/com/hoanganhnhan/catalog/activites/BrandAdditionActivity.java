package com.hoanganhnhan.catalog.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hoanganhnhan.catalog.R;

public class BrandAdditionActivity extends AppCompatActivity {

    ImageButton btnAddBrand;
    TextView txtViewScreenTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_addition);
        setControl();
        setEvent();
    }

    public void setControl(){
        btnAddBrand = findViewById(R.id.btnAdd);
        txtViewScreenTitle = findViewById(R.id.txtViewScreenTitle);
    }

    public void setEvent(){
        btnAddBrand.setVisibility(View.GONE);
        txtViewScreenTitle.setText("Thêm hãng");
    }
}