package com.hoanganhnhan.catalog.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hoanganhnhan.catalog.R;
import com.hoanganhnhan.catalog.models.ItemCategory;
import com.hoanganhnhan.catalog.models.Product;

public class BrandUpdateActivity extends AppCompatActivity {

    ImageButton btnAddBrand;
    TextView txtViewScreenTitle;
    EditText edtUpdateNameBrandManagement;
    ImageView ivUpdateBrandManagement;
    ItemCategory itemBrand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_update);
        setControl();
        setEvent();
    }
    public void setControl(){
        btnAddBrand = findViewById(R.id.btnAdd);
        txtViewScreenTitle = findViewById(R.id.txtViewScreenTitle);
        edtUpdateNameBrandManagement = findViewById(R.id.edtUpdateNameBrandManagement);
        ivUpdateBrandManagement = findViewById(R.id.ivUpdateBrandManagement);
    }

    public void setEvent(){
        btnAddBrand.setVisibility(View.GONE);
        txtViewScreenTitle.setText("Chỉnh sửa");
        itemBrand = getItemBrand();
        edtUpdateNameBrandManagement.setText(itemBrand.getNameCategory());
        ivUpdateBrandManagement.setImageResource(itemBrand.getSrcImage());
    }

    public ItemCategory getItemBrand(){
        Intent intent = getIntent();
        ItemCategory itemBrand = (ItemCategory) intent.getSerializableExtra("itemBrand");
        return itemBrand;
    }
}