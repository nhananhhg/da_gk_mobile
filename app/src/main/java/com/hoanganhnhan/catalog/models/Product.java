package com.hoanganhnhan.catalog.models;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable {
    private int resourceId;
    private String name;
    private String price;
    private List<String> description;
    private List<String> infoConfiguration;

    private String brandId;




    public Product(int resourceId, String name, String price, List<String> description, List<String> infoConfiguration, String brandId) {
        this.resourceId = resourceId;
        this.name = name;
        this.price = price;
        this.description = description;
        this.infoConfiguration = infoConfiguration;
        this.brandId = brandId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getInfoConfiguration() {
        return infoConfiguration;
    }

    public void setInfoConfiguration(List<String> infoConfiguration) {
        this.infoConfiguration = infoConfiguration;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }
}
