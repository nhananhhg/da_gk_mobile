package com.hoanganhnhan.catalog.models;

//dung chung cho brand va demand

import java.io.Serializable;

public class ItemCategory implements Serializable {
    private String idCategory;
    private String nameCategory;
    private int srcImage;

    ItemCategory(){};

    public ItemCategory(String idCategory, String nameCategory, int srcImage) {
        this.idCategory = idCategory;
        this.nameCategory = nameCategory;
        this.srcImage = srcImage;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public int getSrcImage() {
        return srcImage;
    }

    public void setSrcImage(int srcImage) {
        this.srcImage = srcImage;
    }
}
