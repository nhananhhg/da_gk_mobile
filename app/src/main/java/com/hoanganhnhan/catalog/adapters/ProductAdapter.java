package com.hoanganhnhan.catalog.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.hoanganhnhan.catalog.activites.ProductDetail;
import com.hoanganhnhan.catalog.R;
import com.hoanganhnhan.catalog.models.Product;

import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private List<Product> listProducts;
    private Context context;
    public ProductAdapter(Context context) {
        this.context = context;
    }

    public ProductAdapter(Context context, List<Product> listProducts) {
        this.context = context;
        this.listProducts = listProducts;
    }

    public void setData(List<Product> listProducts){
        this.listProducts = listProducts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_product,parent,false);
        return new ProductViewHolder(view);
    }

//    Function set data and show
    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = listProducts.get(position);

        if(product == null){
            return;
        }
        holder.imgProduct.setImageResource(product.getResourceId());
        holder.tvNameProduct.setText(product.getName());
        holder.tvPriceProduct.setText(product.getPrice());

        holder.itemProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoProductDetail(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(listProducts !=null){
            return listProducts.size();
        }
        return 0;
    }

    private void gotoProductDetail(Product product) {
        Intent intent = new Intent(context, ProductDetail.class);
        Bundle bundle = new Bundle();  //tao bundle de truyen du lieu
        bundle.putSerializable("product",product);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNameProduct;
        public TextView tvPriceProduct;
        public ImageView imgProduct;
        public CardView itemProduct;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemProduct = (CardView) itemView.findViewById(R.id.layout_item_product);
            this.tvNameProduct =(TextView) itemView.findViewById(R.id.tv_name_name_layout);
            this.tvPriceProduct =(TextView) itemView.findViewById(R.id.tv_name_price_layout);
            this.imgProduct =(ImageView) itemView.findViewById(R.id.img_product);
        }
    }


}
