package com.hoanganhnhan.catalog.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.hoanganhnhan.catalog.activites.ListCategoryItemActivity;
import com.hoanganhnhan.catalog.R;
import com.hoanganhnhan.catalog.models.ItemCategory;

import java.util.List;

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.BrandViewHolder> {
    private List<ItemCategory> listBrand;
    private Context context;

    public BrandAdapter(Context context) {
        this.context = context;
    }

    public BrandAdapter(Context context, List<ItemCategory> listBrand) {
        this.context = context;
        this.listBrand = listBrand;
    }

    public void setData(List<ItemCategory> listBrand){
        this.listBrand = listBrand;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BrandViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(context).inflate(R.layout.view_holder_brand,parent,false);
        return new BrandViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BrandViewHolder holder, int position) {
       //set data
        ItemCategory itemCataegory = listBrand.get(position);
        if(itemCataegory == null){
            return;
        }
        holder.ivBrand.setImageResource(itemCataegory.getSrcImage());
        holder.cvBrand.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
               // System.out.println(itemCataegory.getIdCategory());
                gotoListProduct(itemCataegory.getIdCategory());  //truyen brand id cua item duoc click
            }
        });
    }

    @Override
    public int getItemCount() {
        if(listBrand !=null){
            return listBrand.size();
        }
        return 0;
    }

    public class BrandViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivBrand;
        private RecyclerView rvBrand;
        public CardView cvBrand;
        public BrandViewHolder(@NonNull View brandView){
            super(brandView);
            ivBrand =brandView.findViewById(R.id.ivBrand);
            rvBrand = brandView.findViewById(R.id.rvBrand);
            cvBrand = brandView.findViewById(R.id.cvBrand);
        }
    }

    private void gotoListProduct(String brandId) { //pthuc được sử dụng để chuyển đến một Activity mới (MainListItemInBrand) với thông tin về brandId,  khi người dùng chọn một item trong danh sách hiển thị
        Intent intent = new Intent(context, ListCategoryItemActivity.class); //Tạo một Intent mới, tham số đầu tiên là context của Adapter và tham số thứ hai là Activity mà sẽ được chuyển đến.
        intent.putExtra("brandId", brandId);  //truyen brandId cua item dc click sang activity
        context.startActivity(intent); //chay activity moi voi intent vưa tao
    }

}
