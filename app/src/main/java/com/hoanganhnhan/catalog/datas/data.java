package com.hoanganhnhan.catalog.datas;

import com.hoanganhnhan.catalog.R;
import com.hoanganhnhan.catalog.models.ItemCategory;
import com.hoanganhnhan.catalog.models.Product;

import java.util.ArrayList;
import java.util.List;

public class data {
    public String[] suggestProduct(){
        String[] PRODUCTS =
                {
                        "MacBook Pro và MacBook Air của Apple",
                        "Dell XPS",
                        "HP Spectre x360",
                        "Lenovo ThinkPad X1 Carbon",
                        "Microsoft Surface Laptop",
                        "Asus ZenBook",
                        "Acer Swift",
                        "Razer Blade Stealth",
                        "MSI Prestige",
                        "LG Gram",
                        "Huawei MateBook",
                        "Samsung Notebook",
                        "Google Pixelbook",
                        "Toshiba Portege"
                };
        return PRODUCTS;
    }

    public List<String> getValueInfoConfiguration() {
        List<String> list = new ArrayList<>();
        list.add("AMD Ryzen 5 5600H");
        list.add("Windows 11");
        list.add("DDR4 8GB 3200MHZ, 2 slot up to 16 GB");
        list.add("Geforce GTX 1650 4GB");
        list.add("15.6 FHD 120HZ IPS ");
        list.add("256GB, SSD NVMe M.2 PCL Gen3");
        list.add("1000Mbps Ethernet");
        list.add("802.11ax 2x2 Wifi Bluetooth");
        list.add("2x USB 3.2 Gen1\n" +
                "1x power connector");
        list.add("4 Zones RGB");
        list.add("45Wh");
        list.add("359.6 x  251.9");
        list.add("2.25kg");
        return list;
    }

    public List<String> getKeyInfoConfiguration() {
        List<String> list = new ArrayList<>();
        list.add("CPU");
        list.add("Hệ điều hành");
        list.add("RAM");
        list.add("GPU");
        list.add("Màn hình");
        list.add("Ổ cứng SSD");
        list.add("LAN");
        list.add("Wireless LAN");
        list.add("Các cổng kết nối");
        list.add("Bàn phím");
        list.add("Pin");
        list.add("Kích thước");
        list.add("Trọng lượng");
        return list;
    }

    public List<ItemCategory> getListBrand(){
        List<ItemCategory> brandItemCataegories;
        brandItemCataegories = new ArrayList<>();
        brandItemCataegories.add(new ItemCategory("1", "MACBOOK", R.drawable.logo_macbook));
        brandItemCataegories.add(new ItemCategory("2", "ASUS",R.drawable.logo_asus));
        brandItemCataegories.add(new ItemCategory("3", "DELL",R.drawable.logo_dell));
        brandItemCataegories.add(new ItemCategory("4", "HP",R.drawable.logo_hp));
        brandItemCataegories.add(new ItemCategory("5", "HUAWEI",R.drawable.logo_huawei));
        brandItemCataegories.add(new ItemCategory("6", "ACER",R.drawable.logo_acer));
        brandItemCataegories.add(new ItemCategory("7", "LENOVO",R.drawable.logo_lenovo));
        brandItemCataegories.add(new ItemCategory("8", "MSI",R.drawable.logo_xiaomi));
        brandItemCataegories.add(new ItemCategory("9", "LG",R.drawable.logo_lg));
        brandItemCataegories.add(new ItemCategory("10", "INTEL",R.drawable.logo_intel));
        return brandItemCataegories;
    }

    public List<ItemCategory> getListDemand(){
        List<ItemCategory> demandItemCataegories;
        demandItemCataegories = new ArrayList<>();
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_vp));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_gaming));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_mong));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_dohoa));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_sv));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_camung));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_vp));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_vp));
        demandItemCataegories.add(new ItemCategory("1", "vanphong",R.drawable.demand_vp));
        return demandItemCataegories;
    }

    public List<String> getDescriptions(){
        List<String> descriptions = new ArrayList<>();
        descriptions.add("Phù hợp cho lập trình viên, thiết kế đồ họa 2D");
        descriptions.add("Hiệu năg vượt trội - Cân mọi tác vụ từ word, excel đến chỉnh sửa ảnh trên các phầm mềm nhưu AI, PTS");
        descriptions.add("Đa nhiệm mượt mà - Ram 8GB cho phép vừa mở trình duyệt để tra cứu thông tin, vừa làm việc trên phần mềm.");
        return descriptions;
    }

    public List<Product> getListProduct(){
        List<Product> products = new ArrayList<>();
        data dataProduct = new data();
        List<String> list = dataProduct.getValueInfoConfiguration();
        List<String> descriptions = getDescriptions();
        products.add(new Product(R.drawable.laptop_1,"Apple MacBook Air M1 256GB 2020 I Chính hãng Apple Việt Nam","19.000.000.d",descriptions,list, "1"));
        products.add(new Product(R.drawable.laptop_2,"Acer Nitro 5 Tiger (2022) Chính Hãng ","17.000.000.d",descriptions,list,"6" ));
        products.add(new Product(R.drawable.laptop_3,"Dell Xps 15 9570 Gen 8th","18.900.000.d",descriptions,list, "3"));
        products.add(new Product(R.drawable.laptop_4,"Asus ExpertBook B9450FA-BM0616R ","19.999.000.d",descriptions,list, "2"));

        products.add(new Product(R.drawable.laptop_1,"Apple MacBook Air M1 256GB 2020 I Chính hãng Apple Việt Nam","19.000.000.d",descriptions,list, "1"));
        products.add(new Product(R.drawable.laptop_2,"Acer Nitro 5 Tiger (2022) Chính Hãng ","17.000.000.d",descriptions,list, "6"));
        products.add(new Product(R.drawable.laptop_3,"Dell Xps 15 9570 Gen 8th","18.900.000.d",descriptions,list, "3"));
        products.add(new Product(R.drawable.laptop_4,"Asus ExpertBook B9450FA-BM0616R ","19.999.000.d",descriptions,list, "2"));

        products.add(new Product(R.drawable.laptop_1,"Apple MacBook Air M1 256GB 2020 I Chính hãng Apple Việt Nam","19.000.000.d",descriptions,list, "1"));
        products.add(new Product(R.drawable.laptop_2,"Acer Nitro 5 Tiger (2022) Chính Hãng ","17.000.000.d",descriptions,list, "6"));
        products.add(new Product(R.drawable.laptop_3,"Dell Xps 15 9570 Gen 8th","18.900.000.d",descriptions,list, "3"));
        products.add(new Product(R.drawable.laptop_4,"Asus ExpertBook B9450FA-BM0616R ","19.999.000.d",descriptions,list, "2"));

        return products;
    }
    }
